<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

<!-- START ABOUT SECTION -->
<section class="home-featured align-items-end d-flex holding_gix">
    <div class="container-fluid px-5 mb-5">
        <div class="row">
            <div class="col-12 col-md-8 wow fadeInUp">
                <h1><strong>COIN SALE TERM SHEET</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                <p class="callout-horizontal"></p>
            </div>
        </div>
</section>
<!-- END ABOUT SECTION -->

<!-- START CHART SECTION -->
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            </div>
        </div>
    </div>
</section>
<!-- END CHART SECTION -->

<!-- START BUSINESS SECTION -->
<section>
    <div class="container-fluid px-5">
        <div class="row text-justify">
            <div class="col-12 col-md-6">
                <p>GoldFinX is building an Ethereum smart contract to support the creation and sale of 1 Billion GiX coins. The coins are sold via different phases. The money raised through each phase is then used to execute Contract Mining Agreements with the chosen mines.</p>
            </div>
            <div class="col-12 col-md-6">
                <p>During Phase I in 2019, 300 Million coins are earmarked to be minted (activated) on the Blockchain and released. GoldFinX expects Phase II around 2021 and Phase III coming into effect in 2023.</p>
            </div>
        </div>
        <div class="row py-5 my-5">
            <div class="col-12 col-md-6">
                <div class="callout-left mb-4">
                    <h3>NUMBER OF COINS ISSUED</h3>
                </div>
                <div class="callout-left-child pb-2">
                    <p class="mb-0 text-brand"><strong>PHASE 1</strong></p>
                    <ul class="pl-4">
                        <li>300 Million coins minted from a total pool of 1 Billion</li>
                        <li>HARD CAP 250 Million coins for up to €250 Million (+50M RESERVED)</li>
                    </ul>
                </div>
                <div class="callout-left-child pb-2">
                    <p class="mb-0 text-brand"><strong>PHASE 2 & 3</strong></p>
                    <p class="font-size-small mt-1">HARD CAP 700 Million coins at market price (Phase II: 2021 & Phase III: 2023)</p>
                </div>
                <div class="callout-left-child pb-2">
                    <p class="mb-0 text-brand"><strong>ROAD MAP TIMELINE</strong></p>
                    <ul class="pl-4">
                        <li>Presale ended on March 31st, 2019.</li>
                        <li>Pre-PCO ended on June 30th, 2019.</li>
                        <li>The GiX PCO (Protected Coin Offering) officially launched on July 1st, 2019.</li>
                    </ul>
                </div>
                <div class="callout-left-child pb-2">
                    <p class="mb-0 text-brand"><strong>GIX COIN PCO PRICE</strong></p>
                    <p class="font-size-small mt-1">GiX face value at PCO of 1 coin= €2</p>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <img src="images/coins-issued-1.svg" class="img-coins-issued-1 wow fadeInRight img-fluid">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <img src="images/coins-issued-2.svg" class="img-coins-issued-2 wow fadeInLeft img-fluid">
            </div>
            <div class="col-12 col-md-6">
<!--            <div class="callout-right-child pb-2">
                    <p class="mb-0 text-brand"><strong>DISCOUNT</strong></p>
                    <p class="font-size-small mt-1">Pre-Sale discounted price: 50% at €1 per GiX (valid up to March 31st, 2019). Maximum number of coins sold at discounted price capped at 250M</p>
                </div>
-->                <div class="callout-right-child pb-2">
                    <p class="mb-0 text-brand"><strong>PAYMENT METHODS</strong></p>
                    <ul class="pl-4">
                        <li>Bitcoin (BTC), Ether (ETH), Ripple (XRP), Tether (USDT)</li>
                        <li>Bank transfers accepted in Euros and US Dollars</li>
                        <li>Cash payments accepted where legal & up to the maximum authorized within country of transaction.<br>
                        (GoldFinX may amend the list of fiat and cryptocurrencies accepted)</li>
                    </ul>
                </div>
                <div class="callout-right-child pb-2">
                    <p class="mb-0 text-brand"><strong>PUBLIC LISTING</strong></p>
                    <p class="font-size-small mt-1">Listing on public and/or distributed crypto-exchanges in Q4, 2019</p>
                </div>
                <div class="callout-right-child pb-2">
                    <p class="mb-0 text-brand"><strong>MINIMUM PURCHASE</strong></p>
                    <p class="font-size-small mt-1">1,000 in fiat and 500 in Crypto</p>
                </div>
                <div class="callout-right-child pb-2">
                    <p class="mb-0 text-brand"><strong>LOCK UP PERIOD</strong></p>
                    <p class="font-size-small mt-1">None for paid coins.</p>
                </div>
                <div class="callout-right-child pb-2">
                    <p class="mb-0 text-brand"><strong>DISTRIBUTION OF 300 MILLION COINS</strong></p>
                    <ul class="pl-4">
                        <li>250 Million to subscribers/buyers through Pre-Sale and PCO</li>
                        <li>50 Million to GoldFinX: Team, Advisors, Services, Partners, Platform & Bounties</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-md-6">
                <div class="callout-left">
                    <p class="text-brand"><strong>USE OF PROCEEDS</strong></p>
                </div>
                <div class="callout-left-child pb-4">
                    <ul class="pl-4">
                        <li>€180M (72%) to Gold mines financing</li>
                        <li>€25M (10%) to GiX Cost of Sales</li>
                        <li>€20M (8%) to GoldFinX’ working capital</li>
                        <li>€7.5M (3%) to GiX marketing and listing</li>
                        <li>€7.5M (3%) to PCO cost</li>
                        <li>€5.0M (2%) to GiX technical platform, security, wallet & App</li>
                        <li>€5.0M (2%) to Heart of Mine Foundation</li>
                    </ul>
                </div>
                <div class="callout-left mt-3">
                    <p class="text-brand"><strong>SMART CONTRACT</strong></p>
                </div>
                <div class="callout-left-child">
                    <p class="font-size-small">Written in Solidity, over Ethereum platform compatible to the Ether ecosystem, and with deployment by GoldFinX in Q3 2019</p>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <img src="images/use-of-proceeds.svg" class="img-useofproceeds img-fluid">
            </div>
        </div>

    </div>
</section>
<!-- END BUSINESS SECTION -->

<?php include 'partials/footer.php'; ?>