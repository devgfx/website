<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

<script src="https://www.google.com/recaptcha/api.js?render=6LcwyqQUAAAAACyRuwJcD9Uiy2rNAzaG5K18YlhF"></script>

<!-- START ABOUT SECTION -->
<section class="home-featured align-items-end d-flex gix_nugget">
    <div class="container-fluid px-5 mb-5">
        <div class="row">
            <div class="col-12 col-md-8 wow fadeInUp">
                <h1>CONTACTS</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                <p class="callout-horizontal"></p>
            </div>
        </div>
</section>
<!-- END ABOUT SECTION -->

<!-- START FAQ SECTION -->
<section>
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-12">
                <div class="container">
                    <h4 class="text-brand">Contact Us</h4>
                    <p>Need support or have a question regarding GoldFinX?</p>
                    <div class="row mt-5">
                        <div class="col-sm-12 col-md-6">
                            <div class="contact-bg">
                                <form action="" class="contact-form">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input id="name" type="text" name="nameContact" class="form-control mb-3" placeholder="Insert Your Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="email" type="text" name="emailContact" class="form-control mb-3" placeholder="Insert Your E-mail">
                                    </div>
                                    <div class="form-group">
                                        <label for="subject" class="">Subject </label>
                                        <select name="subject" class="form-control mb-3">
                                            <option value="" disabled selected>Choose Subject</option>
                                            <option value="Technical Support">Technical Support </option>
                                            <option value="Login Support">Login Support </option>
                                            <option value="Payment Support">Payment Support </option>
                                            <option value="KYC Support">KYC Support </option>
                                            <option value="Need more info">Need more info </option>
                                            <option value="General Comment">General Comment </option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <textarea placeholder="Type Message Here" name="messageContact" id="message" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                    <button class="mt-2 btn btn-primary submit">Send Message</button>
                                </form>

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-5 offset-md-1">
                            <p class="font-size-small mb-4">
                                <strong class="text-brand">GoldFinX PTE LTD - HeadQuarters</strong><br>
                                <i class="fa fa-map-marker"></i> 10 Anson Road #27-08 International Plaza<br>Singapore 079903
                            </p>
                            <p class="font-size-small mb-4">
                                <strong class="text-brand">GoldFinX Inc - America Office</strong><br>
                                <i class="fa fa-map-marker"></i> PO BOX 958,Pasea Estate, Road Town<br>Tortola, British Virgin Islands
                            </p>
                            <p class="font-size-small mb-4">
                                <strong class="text-brand">GoldFinX LTD - Middle East Office</strong><br>
                                <i class="fa fa-map-marker"></i> Office 903<br>Fortune Executive Tower<br>Jumeirah Lake Towers<br>Dubai, United Arab Emirates
                            </p>
                            <p class="font-size-small mb-4">
                                <strong class="text-brand">Email:</strong><br>
                                <i class="fa fa-envelope-o"></i><a href="mailto:info@goldfinx.io">info@goldfinx.io</a> | <a href="mailto:info@goldfinx.com">info@goldfinx.com</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END FAQ SECTION -->

<?php include 'partials/footer.php'; ?>