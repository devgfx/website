<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

    <!-- START ABOUT SECTION -->
    <section class="home-featured align-items-end d-flex fair-trade">
        <div class="container-fluid px-5 mb-5">
            <div class="row">
                <div class="col-12 col-md-8 wow fadeInUp">
                    <h1><strong>FAIR TRADE CRYPTO-FINANCE OFFER</strong></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                    <p class="callout-horizontal"></p>
                </div>
            </div>
    </section>
    <!-- END ABOUT SECTION -->

    <!-- START TEXT SECTION -->
    <section>
        <div class="container-fluid px-5">
            <div class="row">
                <div class="col-md-4">
                    <div class="nav flex-column nav-pills fair-trade-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-agreement-tab" data-toggle="pill" href="#v-pills-agreement" role="tab" aria-controls="v-pills-home" aria-selected="true">Agreement</a>
                        <a class="nav-link" id="v-pills-pre-purchasing-tab" data-toggle="pill" href="#v-pills-pre-purchasing" role="tab" aria-controls="v-pills-pre-purchasing" aria-selected="false">Repayment of Pre-Purchased Gold</a>
                        <a class="nav-link" id="v-pills-operational-control-tab" data-toggle="pill" href="#v-pills-operational-control" role="tab" aria-controls="v-pills-operational-control" aria-selected="false">Operational Control</a>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-agreement" role="tabpanel" aria-labelledby="v-pills-home-tab">
                            <p>The initial funding to the mines, or the Advanced Purchase Amount, is usually on average of €10-15M and covers all the financial requirements of the artisanal mining operation. This allows for vastly increased efficiency and profitability, and more importantly, a sustainable and safe way to operate both socially and environmentally.</p>
                            <p>As part of the Contract Mining agreement, the ASGMs (Artisanal and Small Gold Mines) will not only obtain the necessary funds required for an effective re-boot of their operations but will also benefit from additional logistical support provided from GoldFinX and its board of experts.</p>
                        </div>
                        <div class="tab-pane fade" id="v-pills-pre-purchasing" role="tabpanel" aria-labelledby="v-pills-pre-purchasing-tab">
                            <p>Once each Contract Mining agreement starts operating, the mine will deliver in priority to GoldFinX the pre-purchased gold. This delivery will include a premium of 15%.</p>
                            <p>Once the pre-purchased volume is delivered over a short period of time, the mines will also, as per the agreement, share 20% of their production in gold with GoldFinX for the rest of the economic life of the mine. From this 20%, GoldFinX will route 15% of the refined pure gold to the reserve in Swiss vaults to support the GiX coin and use 5% for its working capital.</p>
                        </div>
                        <div class="tab-pane fade" id="v-pills-operational-control" role="tabpanel" aria-labelledby="v-pills-operational-control-tab">
                            <p>The equipment and systems co-selected and approved by GoldFinX will provide efficient means to monitor and control the production and volume of gold extracted. The mobile stations will be equipped with sensors monitoring; flow of slurry-soil treated, working hours, GPS location and movement, magnetic resistance for gold estimate and other telemetric data, including video and still images of the stations at key areas.</p>
                            <p>Additionally, as per the contract mining agreement, GoldFinX will have on the ground personnel for audits, security and logistics related to the gold expedited to refinery, management support, as well as NGO and local government coordination.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END TEXT SECTION -->

    <!-- START MAP SECTION -->
    <section class="cryptoSlider mt-5">
        <div class="container-fluid px-5">
            <div class="row">
                <div class="col-md-7">
                    <div class="cripto-accordion wow fadeInUp" id="accordion">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <h5 class="collapsed" data-toggle="collapse" data-target="#collapse1">
                                    <span>1</span> GoldFinX PCO
                                </h5>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="col-md-2">
                                            <img src="images/slide/slide_1.svg" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-md-8">
                                            <h6>Fiat + Crypto</h6>
                                            <p>A Protected crypto coin is created using blockchain technology, issued and sold to the public via multi-phase PCOs (Protected Coin Offerings)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row flex-row-reverse">
                <div class="col-md-7">
                    <div class="cripto-accordion wow fadeInUp" id="accordion2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <h5 class="collapsed" data-toggle="collapse" data-target="#collapse2">
                                    <span>2</span> GiX Coin
                                </h5>
                            </div>

                            <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion2">
                                <div class="card-body">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="col-md-2">
                                            <img src="images/slide/slide_2.svg" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-md-8">
                                            <h6>Pre-Purchase</h6>
                                            <p>The proceeds are then used to prepay the gold production of selected qualified ASGM operations for an equivalent amount to satisfactorily cover their CapEx and OpEx needs to start efficient extraction</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="cripto-accordion wow fadeInUp" id="accordion3">
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <h5 class="collapsed" data-toggle="collapse" data-target="#collapse3">
                                    <span>3</span> Operation
                                </h5>
                            </div>

                            <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion3">
                                <div class="card-body">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="col-md-2">
                                            <img src="images/slide/slide_3.svg" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-md-8">
                                            <h6>Gold Mines</h6>
                                            <p>Once the mines start producing "Cleaner Gold", the advanced funds are fully repaid in the form of Gold (normally in less than 12 months) with a premium in weight, and a 20% royalty starts at that point which remains in effect for the life of the mine.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row flex-row-reverse">
                <div class="col-md-7">
                    <div class="cripto-accordion wow fadeInUp" id="accordion4">
                        <div class="card">
                            <div class="card-header" id="heading4">
                                <h5 class="collapsed" data-toggle="collapse" data-target="#collapse4">
                                    <span>4</span> Gold Production
                                </h5>
                            </div>

                            <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion4">
                                <div class="card-body">
                                    <div class="d-flex flex-row align-items-center ">
                                        <div class="col-md-2 col-sm-12">
                                            <img src="images/slide/slide_4.svg" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <h6>Gold Delivery + Production share</h6>
                                            <p>A good portion of the precious metal received by GoldFinX is accrued and stored indefinitely in the vaults of reputable international banks, thus ensuring the protection of the GiX coin.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="cripto-accordion wow fadeInUp" id="accordion5">
                        <div class="card">
                            <div class="card-header" id="heading5">
                                <h5 class="collapsed" data-toggle="collapse" data-target="#collapse5">
                                    <span>5</span> Gold in Vault

                                </h5>
                            </div>

                            <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion5">
                                <div class="card-body">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="col-md-2">
                                            <img src="images/slide/slide_5.svg" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-md-8">
                                            <h6></h6>
                                            <p>The gold in the vault will serve as a protection for all GiX coins issued, thus ensuring a minimum floor price of the GiX on crypto exchnages.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END MAP SECTION -->

<?php include 'partials/footer.php'; ?>