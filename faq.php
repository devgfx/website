<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
<!-- START ABOUT SECTION -->
<section class="home-featured align-items-end d-flex gix_nugget">
    <div class="container-fluid px-5 mb-5">
        <div class="row">
            <div class="col-12 col-md-8 wow fadeInUp">
                <h1>FREQUENTLY ASKED</h1>
                <h1><strong>QUESTIONS</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                <p class="callout-horizontal"></p>
            </div>
        </div>
</section>
<!-- END ABOUT SECTION -->

<!-- START FAQ SECTION -->
<section>
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-md-7">
                <div class="faq-accordion wow fadeInUp" id="accordion">
                    <div class="card">
                        <div class="card-header" id="heading1">
                            <h5 class="collapsed" data-toggle="collapse" data-target="#collapse1">
                                <span>1</span> Will there be a discount/bonus for the Pre-Sale period?
                            </h5>
                        </div>
                        <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">
                            <div class="card-body">
                                The Pre-Sale period terminated on July 1<sup>st</sup>, 2019. The GiX price is now officially at €2. However, until the listing of the GiX on recognized exchanges at the end of this year, selected investors may have a chance to acquire GiX coins at a discounted price.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row flex-row-reverse">
            <div class="col-md-7">
                <div class="faq-accordion wow fadeInUp" id="accordion2">
                    <div class="card">
                        <div class="card-header" id="heading2">
                            <h5 class="collapsed" data-toggle="collapse" data-target="#collapse2">
                                <span>2</span> How can I check the availability of gold in vaults?
                            </h5>
                        </div>

                        <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion2">
                            <div class="card-body">
                                The Gold Reserve is under the exclusive authority of the GiX Trust, which is overseen by an independent Board of Trustees for the benefit of the coin holders. The GiX Trust will produce audit reports on a regular basis and will report on its progression on a monthly basis.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="faq-accordion wow fadeInUp" id="accordion3">
                    <div class="card">
                        <div class="card-header" id="heading3">
                            <h5 class="collapsed" data-toggle="collapse" data-target="#collapse3">
                                <span>3</span> Where can I see my GiX Coins once I purchase them?
                            </h5>
                        </div>

                        <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion3">
                            <div class="card-body">
                                All GiX will be stored in your GiX Wallet once you create an account directly from our website. The GiX Wallet is only accessible with your own username and password. In Q4 of 2019, you will have the option to transfer your coins directly from your GiX Wallet to any ERC-20 compatible wallet for storage and eventual trading on exchnages.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row flex-row-reverse">
            <div class="col-md-7">
                <div class="faq-accordion wow fadeInUp" id="accordion4">
                    <div class="card">
                        <div class="card-header" id="heading4">
                            <h5 class="collapsed" data-toggle="collapse" data-target="#collapse4">
                                <span>4</span> What are cryptocurrencies?
                            </h5>
                        </div>

                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion4">
                            <div class="card-body">
                                Cryptocurrencies are digital, decentralized monetary structures, created via blockchain technology. Most cryptocurrencies can be exchanged for fiat currencies (USD, EUROS, YEN) or other cryptocurrencies like Bitcoin or Ether.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="faq-accordion wow fadeInUp" id="accordion5">
                    <div class="card">
                        <div class="card-header" id="heading5">
                            <h5 class="collapsed" data-toggle="collapse" data-target="#collapse5">
                                <span>5</span> Will GiX be subject to cryptocurrency volatility?
                            </h5>
                        </div>

                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion5">
                            <div class="card-body">
                                GiX is a cryptocurrency, and like any other crypto or fiat currency, it will fluctuate. The case of the GiX is a bit different as it’s protected by a physical Gold asset sitting in secured vaults. The Gold captively accumulating in the vault supports the value of the coin, making the investment a more secure option than a typical coin offering.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row flex-row-reverse">
            <div class="col-md-7">
                <div class="faq-accordion wow fadeInUp" id="accordion6">
                    <div class="card">
                        <div class="card-header" id="heading6">
                            <h5 class="collapsed" data-toggle="collapse" data-target="#collapse6">
                                <span>6</span> How many GiX Coins will be issued?
                            </h5>
                        </div>

                        <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion6">
                            <div class="card-body">
                                GoldFinX is creating 1 Billion coins, and 250 million coins will be sold during Phase I.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="faq-accordion wow fadeInUp" id="accordion7">
                    <div class="card">
                        <div class="card-header" id="heading7">
                            <h5 class="collapsed" data-toggle="collapse" data-target="#collapse7">
                                <span>7</span> When can the GiX Coin be traded?
                            </h5>
                        </div>

                        <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion7">
                            <div class="card-body">
                                GiX, like any cryptocurrency, will be tradable and exchangeable on public crypto exchanges. Every year more exchanges are created to accommodate the volume and diversity of the new coins released. We anticipate having the GiX trading on major exchanges (Binance, Huobi, Kucoin) in December, 2019.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row flex-row-reverse">
            <div class="col-md-7">
                <div class="faq-accordion wow fadeInUp" id="accordion8">
                    <div class="card">
                        <div class="card-header" id="heading8">
                            <h5 class="collapsed" data-toggle="collapse" data-target="#collapse8">
                                <span>8</span> Which ecosystem platform will GoldFinX use?
                            </h5>
                        </div>

                        <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion8">
                            <div class="card-body">
                                GoldFinX will use the Ethereum platform (ERC20 token standard). Industry experts note that such tokens are quite popular, and they are supported by almost all crypto exchanges and wallets that meet this standard.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center my-5 py-5">
            <a href="documents/GoldFinX_faq_V1.0.pdf" target="_blank" class="btn btn-primary">CLICK TO DOWNLOAD <span>ALL FAQ</span></a>
        </div>
    </div>
</section>
<!-- END FAQ SECTION -->

<?php include 'partials/footer.php'; ?>