<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

    <!-- START ABOUT SECTION -->
    <section class="home-featured align-items-end d-flex gold-mining">
        <div class="container-fluid px-5 mb-5">
            <div class="row">
                <div class="col-12 col-md-8 wow fadeInUp">
                    <h1><strong>GiX UNIQUE CONCEPT</strong></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                    <p class="callout-horizontal"></p>
                </div>
            </div>
    </section>
    <!-- END ABOUT SECTION -->

    <!-- START GRAPHIC SECTION -->
    <section>
        <div class="container-fluid p-0">
            <img src="images/unique-concept.svg" class="img-uniqueconcept">
        </div>
    </section>
    <!-- END GRAPHIC SECTION -->

    <!-- START CONCEPT SECTION -->
    <section class="">
        <div class="container-fluid px-5">
            <div class="row">
                <div class="col-12">
                    <div class="callout-left mb-5">
                        <h3><strong>GiX</strong> is the resulting blend of a <strong>solid business model</strong> concretely rooted in a proven, profitable industry—with the convenience of being a cryptocurrency.</h3>
                    </div>
                </div>
            </div>

            <div class="row text-justify">
                <div class="col-12 col-md-6">
                    <p class="callout-left-child">The Exclusive concept behind the GoldFinX Contract Mining Agreement, which includes a production share of 20% of the gold mined, guarantees a systematic way of increasing the value of the gold that is continuously being stored in a world-renowned vault.</p>
                    <p class="callout-left-child">This greater value should in turn increase the trading value of the GiX coins.</p>
                </div>
                <div class="col-12 col-md-6">
                    <p class="callout-right-child">Should a major catastrophe impede the life cycle of the GiX coins, the GiX coin holder can have the peace of mind, in a case of dissolution, that they would also be the recipient of the value of the gold stored in the vault in the same proportion of their coin ownership in respect to the entire pool of GiX coins.</p>
                    <p class="callout-right-child">Gold has always been a historical safe haven investment that increases throughout time.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- END CONCEPT SECTION -->

<?php include 'partials/footer.php'; ?>