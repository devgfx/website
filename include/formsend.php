<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

$mail = new PHPMailer(true);

date_default_timezone_set('Europe/Lisbon'); // Timezone default

// include de valores default
$defaultEmailTo = "no-reply@goldfinx.com"; 


// Only process POST reqeusts.
if ($_SERVER["REQUEST_METHOD"] == "POST") {
   
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                       // Enable verbose debug output
		$mail->isSMTP();                                            // Set mailer to use SMTP
		$mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
		$mail->Username   = $defaultEmailTo;                     // SMTP username
		$mail->Password   = 'Franc1sca!1234';                               // SMTP password
		$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
		$mail->Port       = 587;                                    // TCP port to connect to
		
		$subject = strip_tags(trim($_POST['subject']));
		
		$name = strip_tags(trim($_POST["nameContact"]));
		$name = str_replace(array("\r","\n"),array(" "," "),$name);

		$email = filter_var(trim($_POST["emailContact"]), FILTER_SANITIZE_EMAIL);
		$message = trim($_POST["messageContact"]);
		
		$email_content = "Name: $name<br>";
		$email_content .= "Email: $email<br>";
		$email_content .= "Message:<br>$message<br>";

		
		$mail->setFrom($defaultEmailTo, 'GoldFinX [Website]');
		$mail->addAddress("ac@goldfinx.com", $name);     // Add a recipient
		
		$mail->isHTML(true);
		$mail->Subject = $subject;
		
		$mail->Body    = $email_content;

		if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL) OR empty($subject)) {
			// Set a 400 (bad request) response code and exit.
			http_response_code(400);
			echo "Oops! There was a problem with your submission. Please complete the form and try again.2";
			exit;
		}else{
			$mail->send();
			
			http_response_code(200);
			echo "Thank You! Your message has been sent.";	
		}
		
	} catch (Exception $e) {
		http_response_code(500);
        echo "Oops! Something went wrong and we couldn't send your message.";
	}    

} else {
    // Not a POST request, set a 403 (forbidden) response code.
    http_response_code(403);
    exit();
}


?>
