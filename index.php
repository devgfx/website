<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

<!-- START EMPOWERING SECTION -->
<section class="home-featured align-items-end d-flex">
    <video width="100%" autoplay muted loop id="video-desktop">
        <source src="images/video-home-new.mp4" type="video/mp4">
    </video>
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-lg-8 col-xl-6 wow fadeInUp">
                <h1><strong>EMPOWERING ARTISANAL GOLD MINING</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                <p class="callout-horizontal">Join our Protected Coin Offering and put gold mines in your wallet</p>
            </div>
        </div>
        <div class="row in-the-news">
            <div class="col-12 col-lg-11 col-xl-10 mx-auto">
                <div class="row text-center align-items-center justify-content-center">
                <h5 class="mb-0 px-4">GoldFinX <span>IN THE NEWS</span></h5>
                <img src="images/media/mini-forbes.png">
                <img src="images/media/mini-ccn.png">
                <img src="images/media/mini-investor-place.svg">
                <img src="images/media/mini-entrepreneur.svg">
                <div class="px-4">
                    <a href="media.php" class="btn btn-primary">+ Read All</a>
                </div>
                    <!-- <div class="col">
                        <h5 class="mb-0">GoldFinX In The News</h5>
                    </div>
                    <div class="col px-0">
                        <img src="images/media/forbes.png">
                    </div>
                    <div class="col px-0">
                        <img src="images/media/ccn.png">
                    </div>
                    <div class="col px-0">
                        <img src="images/media/investor-place.svg">
                    </div>
                    <div class="col px-0">
                        <img src="images/media/entrepreneur.svg">
                    </div>
                    <div class="col pr-0">
                        <a href="media.php" class="btn btn-primary">+ Read All</a>
                    </div> -->
                </div>
            </div>
        </div>
</section>
<!-- <img src="images/sprinkles.png" class="img-sprinkles" style="width:100%;"> -->
<!-- <div class="home-featured-buy-button">
    <button type="button" class="btn btn-warning btn-lg">BUY <img src="images/gix-btn.svg" class="img-gixbtn"></button>
</div> -->
<section class="container-fluid px-5">
        <div class="row py-5 my-2">
            <div class="col-12 col-md-8 col-lg-6">
                <div class="callout-left wow fadeInLeft">
                    <h3>Thousands of tons of Gold waiting to be extracted</h3>
                </div>
                <p class="callout-left-child wow fadeInUp" data-wow-delay="1s">Artisanal Gold Mines around the world are undervalued and have a negative social and environmental impact as they operate under archaic, inhumane, and toxic conditions.</p>
            </div>
        </div>

        <div class="row py-5 my-2 mb-5">
            <div class="col-12 col-md-4 col-lg-5">
                <img src="images/broken-nugget.png" class="img-brokennugget">
            </div>
            <div class="col-12 col-md-8 col-lg-7 px-0 pl-md-1">
                <div class="callout-right text-right wow fadeInRight">
                    <h3>The Opportunity comes with Responsibility</h3>
                    <h3><strong>DO GOOD, RECEIVE GOOD...</strong></h3>
                </div>
                <div class="callout-right-child wow fadeInUp text-justify" data-wow-delay="1s">
                    <p>We provide adequate capital to Artisanal Gold Mines worldwide while helping them to employ socially conscious and sustainable mining methods that reduce the negative impact on the surrounding ecological systems. In exchange, we get a Lifetime Royalty share of ALL ongoing production.</p>
                </div>
            </div>
        </div>

        <div class="row  py-5 my-2">
            <div class="col-12 col-md-6 wow fadeInLeft">
                <div class="callout-left">
                    <h3>In the form of Fair Trade Advanced-Purchase Agreements, we obtain:</h3>
                </div>
            </div>
            <div class="col-12 col-md-3 wow fadeInUp" data-wow-delay="1s">
                <div class="text-center">
                    <div class="advanced-purchase-agreement">
                        <img src="images/nugget.svg" class="img-nuggetagreement">
                        15<span>%</span>
                    </div>
                    <p>Premium on the Initial Funding</p>
                </div>
            </div>
            <div class="col-12 col-md-3 wow fadeInUp" data-wow-delay="2s">
                <div class="text-center">
                    <div class="advanced-purchase-agreement">
                        <img src="images/nugget.svg" class="img-nuggetagreement">
                        20<span>%</span>
                    </div>
                    <p>Lifetime Royalty share of ALL Ongoing Production</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END EMPOWERING SECTION -->

<!-- START HOW WE DO IT SECTION -->
<section>
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft">
                <h3><strong>HOW WE DO IT?</strong></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 text-justify">
                <div class="callout-horizontal wow fadeInLeft mb-5 pb-5" >
                    <div class="watermarked">
                        <h5>GoldFinX</h5>
                        <p><strong>GoldFinX</strong> created a Cryptocurrency, <strong>GiX</strong>, which is built on top of the Ethereum Blockchain using the ERC-20 protocol.</p>
                    </div>
                    <div class="watermarked">
                        <h5>GiX</h5>
                        <p><strong>GiX</strong> will be sold through a PCO and will use its proceeds as a vehicle to adequately fund the carefully selected mining operations worldwide through a <strong>GOLD</strong> pre-purchasing agreement..</p>
                    </div>
                    <div class="watermarked">
                        <h5>GOLD</h5>
                        <p>The <strong>GOLD</strong> extraction will repay the funding with a 15% premium and a 20% Royalty throughout the mine lifecycle, and will be stored in the form of purified <strong>GOLD</strong> to protect the value of <strong>GiX</strong>. </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <img src="images/coin-front-mono.png" class="img-coinfrontmono">
            </div>
        </div>

        <div class="row align-items-center py-5 my-5">
            <div class="col-12 col-md-4 wow fadeInLeft" data-wow-delay="1s">
                <div class="img-philippequote">
                    <img src="images/philippe-quote.jpg">
                </div>
            </div>
            <div class="col-12 col-md-8 wow fadeInUp text-right">
                <h3 class="font-italic">“<strong>GiX</strong> will unlock the <strong>Billions</strong> of Euros worth of untapped <strong>GOLD</strong> in this artisanal mining segment with <strong>Environment</strong> preservation in mind.”</h3>
                <h6 class="text-right">GoldFinX CEO - Philippe Bednarek</h6>
            </div>
        </div>

        <div class="row pt-5">
            <div class="col-12">
                <div class="callout-left wow fadeInLeft">
                    <h3>A Protected Crypto-Coin Created for Everyone!</h3>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12 col-md-7 wow fadeInUp" data-wow-delay="1s">
                <img src="images/gix-3d.png" class="img-gix3d">
            </div>
            <div class="col-12 col-md-5 pt-5 wow fadeInUp" data-wow-delay="1.5s">
                <div>
                    <h4 class="font-weight-light">Legally Protected<br><strong class="text-brand">Reserve / Price of GiX</strong></h4>
                </div>
                <div class="mt-3">
                    <h4 class="font-weight-light">Permanent Gold Storage<br><strong class="text-brand">Renowned Swiss Vaults</strong></h4>
                </div>
                <div class="mt-3">
                    <h4 class="font-weight-light">Risk Diversification<br><strong class="text-brand">75+ Targeted Mines, Co-Op Partnerships</strong></h4>
                </div>
                <div class="mt-3">
                    <h4 class="font-weight-light">Minimum Correlation<br><strong class="text-brand">Reserve / Price of GiX</strong></h4>
                </div>
                <div class="mt-3">
                    <h4 class="font-weight-light">GiX Sold to Date<br><strong class="text-brand">11,218,511</strong></h4>
                </div>
            </div>
        </div>
        </div>
</section>
<!-- END HOW WE DO IT SECTION -->

<!-- START ROADMAP SECTION -->
<section class="roadmap-section">
    <div class="container-fluid px-5 mt-5">
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft">
                <h3><strong>ROADMAP</strong></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="callout-horizontal wow fadeInLeft mb-md-5 pb-md-5 pt-5" data-wow-delay="1s"></div>
            </div>
        </div>
        <!-- START ROADMAP GRAPHIC -->
        <div class="roadmap wow fadeInRight clearfix">
            <div class="time-mark wow fadeInDown" data-wow-delay="2s">
                <div class="year"><span>Q1 2017</span></div>
                <div class="title">CONCEPT VALIDATION</div>
                <div class="description">Validation of fundamentals (Blockchain, financial, social)</div>
            </div>
            <div class="time-mark  wow fadeInUp" data-wow-delay="2.5s">
                <div class="year">Q2 2017<span></span></div>
                <div class="title">PROJECT VALIDATION</div>
                <div class="description">Project validated, start building the organization & research mines</div>
            </div>
            <div class="time-mark wow fadeInDown" data-wow-delay="3s">
                <div class="year">Q1 2018<span></span></div>
                <div class="title">PCO PREP</div>
                <div class="description">Structure Smart Contract, processes & organization staffing</div>
            </div>
            <div class="time-mark  wow fadeInUp" data-wow-delay="3.5s">
                <div class="year">Q4 2018<span></span></div>
                <div class="title">PRE-SALE 1 GIX = €1</div>
                <div class="description">Organize the private sale of private of GiX</div>
            </div>
            <div class="time-mark wow fadeInDown" data-wow-delay="4s">
                <div class="year">Q3 2019<span></span></div>
                <div class="title">PCO LAUNCH 1 GIX = €2</div>
                <div class="description">Launch official public sale for remaining GiX coins</div>
            </div>
            <div class="time-mark  wow fadeInUp upcoming" data-wow-delay="4.5s">
                <div class="year">Q4 2019<span></span></div>
                <div class="title">GOLD PRODUCTION</div>
                <div class="description">Initial mines funded; Gold extraction & stored in vault</div>
            </div>
            <div class="time-mark wow fadeInDown upcoming" data-wow-delay="5s">
                <div class="year">Q4 2019<span></span></div>
                <div class="title">GIX LISTING</div>
                <div class="description">GiX Coin to be traded on crypto-exchanges</div>
            </div>
            <div class="time-mark  wow fadeInUp upcoming" data-wow-delay="5.5s">
                <div class="year">Q1 2021<span></span></div>
                <div class="title">PHASE 2 PCO</div>
                <div class="description">Repeat Phase I, prep, sale, funding mines, receive gold in vaul</div>
            </div>
            <div class="time-mark wow fadeInDown upcoming" data-wow-delay="6s">
                <div class="year">Q1 2023<span></span></div>
                <div class="title">PHASE 3 PCO</div>
                <div class="description">Repeat Phase II, sale, funding mines, receive gold in vault</div>
            </div>
            <div class="timeline"></div>
        </div>
        <!-- END ROADMAP GRAPHIC -->
    </div>
</section>
<!-- END ROADMAP SECTION -->

<!-- START TEAM -->
<section class="team" id="management-team">
    <div class="container-fluid px-5 pt-5">
        <div class="row my-2">
            <div class="col-12 col-md-6 pl-0">
                <div class="callout-left wow fadeInLeft">
                    <h3><strong>MANAGEMENT TEAM</strong></h3>
                    <p class="text-justify">Our team offers a unique combination of extensive business experience, technical skills, international perspective and hands-on attitude. We blend deep technological skills with creativity and academic excellence with executive experience. Our aim is to establish ourselves as a pioneering leader within the crypto-currency industry.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 member">
                <img src="images/team/t1.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>President</small></p>
                <h5>JEAN-MICHEL ALFIERI</h5>
                <a href="https://www.linkedin.com/in/jean-michel-alfieri-5234b343" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:jm@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 member">
                <img src="images/team/t2.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Chief Executive Officer</small></p>
                <h5>PHILIPPE BEDNAREK</h5>
                <a target="_blank" href="https://www.linkedin.com/in/philippe-bednarek-82462b13">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:pb@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t3.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Chief Operating Officer</small></p>
                <h5>FRANCOIS DUMONT</h5>
                <a href="https://www.linkedin.com/in/francoiswdumont" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:fd@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t4.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Chief Information Officer</small></p>
                <h5>AMYN CHAGAN</h5>
                <a href="https://ca.linkedin.com/in/amynchagan" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:ac@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t5.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Chief Community Officer</small></p>
                <h5>ADAM BOULEL</h5>
                <a href="https://www.linkedin.com/in/phi-phi-more-301525176/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:ab@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t6.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Senior VP Marketing</small></p>
                <h5>GONZALO GANDIA</h5>
                <a href="https://www.linkedin.com/in/gonzalo-gandia-b590aa27" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:gg@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t7.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Chief Technology Officer</small></p>
                <h5>CHARBEL HAGE</h5>
                <a href="https://www.linkedin.com/in/charbel-hage-774225a4" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:ch@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t8.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>General Counsel</small></p>
                <h5>DEBORA TURNER</h5>
                <a href="https://www.linkedin.com/in/debora-turner-7b14544" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:dt@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t9.png" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>VP Business Development</small></p>
                <h5>PHI-PHI MORE</h5>
                <a href="https://www.linkedin.com/in/phi-phi-more-301525176/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:ab@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t10.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Media Manager</small></p>
                <h5>MICHAEL YEUNG</h5>
                <a href="https://www.linkedin.com/in/yeung-michael-719931182/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:my@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/team/t11.png" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Legal Counsel</small></p>
                <h5>CLAUDE MIZRAHI</h5>
                <a href="https://www.linkedin.com/in/claude-benjamin-mizrahi-3892b324/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:cm@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
        </div>
        <!-- END TEAM -->

        <!-- START ADVISORY -->
        <div class="row py-3 pt-5 mt-5">
            <div class="col-12 col-md-6 pl-0">
                <div class="callout-left wow fadeInLeft">
                    <h3 class="text-left"><strong>Advisory Board</strong></h3>
                    <p class="text-justify">Our Advisory Board has decades of academic and real-world experience in their respective fields. Our Mining Advisors provide high expertise on reserve estimation, mining technology, and mining management. Our Finance Advisors bring extensive experience in the Financial and crypto markets, and have acted as consultants and key advisors for many international companies, including successful Cryptocurrency and Blockchain businesses.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 member">
                <img src="images/advisory/a1.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Mining Advisor</small></p>
                <h5>MARTA BENITO</h5>
                <a href="https://www.linkedin.com/in/martabenito" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 member">
                <img src="images/advisory/a2.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Mining Advisor</small></p>
                <h5>ALAIN BERCLAZ</h5>
                <a href="https://www.linkedin.com/in/alainberclaz" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a3.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Mining Advisor</small></p>
                <h5>ALDO GOROVATSKY</h5>
                <a href="https://www.linkedin.com/in/aldo-gorovatsky-058a1512b/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a4.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Mining Advisor</small></p>
                <h5>FERNANDO GALLARDO</h5>
                <a href="https://www.linkedin.com/in/fernando-gallardo-falconi-16643345" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a5.png" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Finance Advisor</small></p>
                <h5>DR. HEINZ KUBLI</h5>
                <a href="https://www.linkedin.com/in/heinz-kubli-295132/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a6.png" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>PCO Advisor</small></p>
                <h5>TIMO TRIPPLER</h5>
                <a href="https://www.linkedin.com/in/timotrippler/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a7.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Economic Advisor</small></p>
                <h5>GRAHAM LEACH</h5>
                <a href="https://www.linkedin.com/in/grahamrleach/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a8.jpg" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Investment Advisor MEA</small></p>
                <h5>JASSEM OSSEIRAM</h5>
                <a href="https://www.linkedin.com/in/jassem-osseiran-a4623127/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a9.png" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small> Business Development Advisor</small></p>
                <h5>CHRISTIAN BEAUX</h5>
                <a href="https://www.linkedin.com/in/christianbeaux/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:cb@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a10.png" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Business Development Advisor</small></p>
                <h5>THIERRY LOUSTAU</h5>
                <a href="https://www.linkedin.com/in/thierry-loustau-b2973845/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:tl@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a11.png" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Strategic Advisor</small></p>
                <h5>DATO’ AHMAD HISHAM</h5>
                <a href="https://www.linkedin.com/in/dato-ahmad-hisham/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md member">
                <img src="images/advisory/a12.png" class="rounded-circle img-fluid photo">
                <p class="mb-0"><small>Investment Advisor</small></p>
                <h5>CHRISTOPHER PONNIAH</h5>
                <a href="https://www.linkedin.com/in/chris-ponniah-19107943/" target="_blank">
                    <img src="images/linkedin-brands.svg" class="img-fluid">
                </a>
                <a href="mailto:cp@goldfinx.com">
                    <img src="images/at-solid.svg" class="img-fluid">
                </a>
            </div>
        </div>
        <!-- END ADVISORY -->
    </div>
</section>


<!-- START NEWS SECTION -->
<!-- <section class="mt-5 pt-5">
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft">
                <h3><strong>GoldFinX Updates</strong></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="callout-horizontal wow fadeInLeft mb-3 pb-3" data-wow-delay="1s"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 d-flex justify-content-center">
                <div class="card news-item">
                    <svg class="card-img-top" width="200" height="200" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 200x200">
                        <title>TITLE</title>
                        <rect width="100%" height="100%" fill="#868e96"></rect>
                        <text x="50%" y="50%" fill="#dee2e6" dy=".3em">200x200</text>
                    </svg>
                    <div class="card-body">
                        <p class="date">MAY 05, 2019</p>
                        <p class="title">Card title</p>
                        <p class="description">GoldFinX is raising funds through a Initial Coin Offering (PCO) to provide financing to ASGM miners for more successful, sustainable, and safer mining operations.</p>
                        <div class="text-right">
                            <a href="#" class="btn btn-primary">+ Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 d-flex justify-content-center">
                <div class="card news-item">
                    <svg class="card-img-top" width="200" height="200" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 200x200">
                        <title>TITLE</title>
                        <rect width="100%" height="100%" fill="#868e96"></rect>
                        <text x="50%" y="50%" fill="#dee2e6" dy=".3em">200x200</text>
                    </svg>
                    <div class="card-body">
                        <p class="date">MAY 05, 2019</p>
                        <p class="title">Card title</p>
                        <p class="description">GoldFinX is raising funds through a Initial Coin Offering (PCO) to provide financing to ASGM miners for more successful, sustainable, and safer mining operations.</p>
                        <div class="text-right">
                            <a href="#" class="btn btn-primary">+ Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 d-flex justify-content-center">
                <div class="card news-item">
                    <svg class="card-img-top" width="200" height="200" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 200x200">
                        <title>TITLE</title>
                        <rect width="100%" height="100%" fill="#868e96"></rect>
                        <text x="50%" y="50%" fill="#dee2e6" dy=".3em">200x200</text>
                    </svg>
                    <div class="card-body">
                        <p class="date">MAY 05, 2019</p>
                        <p class="title">Card title</p>
                        <p class="description">GoldFinX is raising funds through a Initial Coin Offering (PCO) to provide financing to ASGM miners for more successful, sustainable, and safer mining operations.</p>
                        <div class="text-right">
                            <a href="#" class="btn btn-primary">+ Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</section> -->
<!-- END NEWS SECTION -->
<?php include 'partials/footer.php'; ?>