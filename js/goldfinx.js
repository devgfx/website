// var sprinkles = $(".img-sprinkles")[0];
// var options = {
//     orientation: 'up',
//     overflow:true,
//     scale:1.8
// };
// new simpleParallax(sprinkles,options);

// var philippequote = $(".img-philippequote img")[0];
// var options = {
//     orientation: 'up',
//     scale:1,
//     delay:10
// };
// new simpleParallax(philippequote,options);

$(document).ready(function(){

    setTimeout(function(){
        $(".img-sprinkles").fadeIn();
    },2000);


    var wow = new WOW({
        callback: afterReveal
    });

    var afterReveal = function(e){
        debugger;
    }

    wow.init();

    $(".navbar .dropdown-menu").on("shown.bs.dropdown",function(e){
        $(e.currentTarget).parents(".navbar").addClass("open")
    }).on("hide.bs.dropdown",function(e){
        $(e.currentTarget).parents(".navbar").removeClass("open")
    });


    $("#navbarNav").on("show.bs.collapse",function(e){
        $(".navbar").addClass("mobile-open");
    }).on("hide.bs.collapse",function(e){
        $(".navbar").removeClass("mobile-open");
    });


    $('[data-behave="open-layer"]').on("click",function(e){
        e.preventDefault();
        var target = $(this).attr("data-target");
        $(target).slideDown();
    });

    $('[data-behave="close-layer"]').on("click",function(e){
        e.preventDefault();
        var target = $(this).attr("data-target");
        $(target).slideUp();
    });

    resizeRoadmap(true);

    $(".faq-accordion .collapse").on("shown.bs.collapse",function(e){
        var aux = $(e.currentTarget).parents(".faq-accordion");
        $('html,body').animate({
            scrollTop: aux.offset().top - 230
         }, 500);
    });

    $(".cripto-accordion .collapse").on("shown.bs.collapse",function(e){
        var aux = $(e.currentTarget).parents(".cripto-accordion");
        $('html,body').animate({
            scrollTop: aux.offset().top - 230
         }, 500);
    });


    $(window).trigger("scroll");


    var sending = false;
	$(".submit").click(function(e){
		e.preventDefault();
		if(sending == true) return;
        sending = true;
        
        grecaptcha.ready(function() {
            grecaptcha.execute('6LcwyqQUAAAAACyRuwJcD9Uiy2rNAzaG5K18YlhF', {action: 'homepage'}).then(function(token) {
                $("#ContactSubmit").html("Sending...");
                $("#modalContact .alert").remove();
                $.ajax({
                    type: "POST",
                    url: "include/formsend.php",
                    data: $(".contact-form").serialize(),
                    
                    
                    success: function(data){
                        $(".contact-form").prepend("<div class='alert alert-success'>Message sent successfuly</div>")
                    },
                    error:function(data){
                        $(".contact-form").prepend("<div class='alert alert-danger'>Message not sent</div>")
                    },
                    complete:function(data)
                    {
                        sending = false;
                        $("#ContactSubmit").html("Send");
                    }
                });
            });
        });
    });
    
});

function resizeRoadmap(launch){

    $(".roadmap .time-mark").each(function(i,obj){
        if($(window).width() > 991 ){
            var aux = (launch)? $(obj).outerWidth() * i : $(obj).outerWidth()/2 * i;
            $(this).css({
                "left":aux,
                "width":"22%",
            });
            if(i==5){
                $(".roadmap .timeline").attr("future-width",aux);
            }
        }
    });
}

$(window).scroll(function(){
    // This is then function used to detect if the element is scrolled into view
    function elementScrolled(elem)
    {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(elem).offset().top;
        return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
    }

    if($('.roadmap').length > 0){
        if(elementScrolled('.roadmap')) {
            setTimeout(function(){
                var aux = $(".roadmap .timeline").attr("future-width");
                $(".roadmap .timeline").css("width",aux);  
            },1900)
        }
    }

    if($(window).scrollTop() > 0){
        $(".in-the-news").addClass("invisible");
    }
    if($(window).scrollTop() == 0){
        $(".in-the-news").removeClass("invisible");
    } 
});

$(window).on("resize",function(){
    resizeRoadmap();
});