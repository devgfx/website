<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

<!-- START ABOUT SECTION -->
<section class="home-featured align-items-end d-flex gix_nugget">
    <div class="container-fluid px-5 mb-5">
        <div class="row">
            <div class="col-12 col-md-8 wow fadeInUp">
                <h1>GOLDFINX</h1>
                <h3><strong>IN THE NEWS</strong></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                <p class="callout-horizontal"></p>
            </div>
        </div>
</section>
<!-- END ABOUT SECTION -->
<!-- START NEWS SECTION -->
<section>
    <div class="container-fluid px-5">
        <div class="row pb-4">
        <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                    <img src="./images/media/forbes.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Sep 20, 2019</p>
                        <p class="title">Forbes</p>
                        <p class="description">"6 Startups Flourishing In Markets Outside The U.S..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://www.forbes.com/sites/joresablount/2019/09/20/6-startups-flourishing-in-markets-outside-the-u-s/#290df1273854" target="_blank" class="btn btn-primary">+ Read More</a>
                        </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                <img src="./images/media/coinspeaker.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">AUG 16, 2019</p>
                        <p class="title">Coinspeaker </p>
                        <p class="description">"A Paradigm Shift: Fintech as a Force for Good in the Small-Scale Gold Sector"</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://www.coinspeaker.com/a-paradigm-shift-fintech-as-a-force-for-good-in-the-small-scale-gold-sector/" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                </div>
            </div>

            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                    <img src="./images/media/nasdaq.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Aug 01, 2019</p>
                        <p class="title">Nasdaq</p>
                        <p class="description">"Major artisanal gold mining deal kicks off a revolutionary green gold initiative led by <strong>GoldFinX</strong>"</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://www.nasdaq.com/press-release/major-artisanal-gold-mining-deal-kicks-off-a-revolutionary-green-gold-initiative-led-by-goldfinx-20190801-00164" target="_blank" class="btn btn-primary">+ Read More</a>
                        </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                    <img src="./images/media/entrepreneur.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Apr 16, 2019</p>
                        <p class="title">Entrepreneur</p>
                        <p class="description">"<strong>GoldFinX</strong>: Can a Fintech Change the Fortunes of Artisanal Gold Mining???"</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://www.entrepreneur.com/article/332345" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                    
                </div>
            </div>

        </div>
        <div class="row pb-4">
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card p-3 bg-warning news-item">
                    <blockquote class="blockquote mb-0">
                        <h2>“Gold and crypto combined in a single resource currently in presale brought by GoldFinX”</h2>
                        <div class="blockquote-footer text-right">
                            <small class="text-muted">
                                FORBES <cite title="Source Title"></cite>
                            </small>
                        </div>
                    </blockquote>
                </div>
            </div>
        <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                    <div class="card news-item w-100" style="background:url(images/GoldFactsHeader.png) no-repeat center center;background-size:cover">&nbsp;</div>
                </div>
            
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                    <img src="./images/media/bitcoin-warrior.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Jul 26, 2019</p>
                        <p class="title">Bitcoin Warrior</p>
                        <p class="description">"<strong>GoldFinX</strong> Supports Artisanal Gold Miners with their Gold-Protected Token, GiX"</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://bitcoinwarrior.net/2019/07/goldfinx-supports-artisanal-gold-miners-with-their-gold-protected-token-gix/" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                    <img src="./images/media/hot-crypto-news.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Sep 10, 2019</p>
                        <p class="title">Hotcrypto news</p>
                        <p class="description">"<strong>GoldFinX</strong> Presented With The Best Community Award"</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://hotcryptonews.com/2019/09/10/goldfinx-presented-with-the-best-community-award/" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                    <img src="./images/media/techno-faq.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Aug 02, 2019</p>
                        <p class="title">Technofaq</p>
                        <p class="description">"<strong>GoldFinX</strong> lets you participate in actual gold mining"</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://technofaq.org/posts/2019/08/goldfinx-lets-participate-actual-gold-mining/" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                <img src="./images/media/ccn.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Mar 02, 2019</p>
                        <p class="title">CCN</p>
                        <p class="description">"Why Accumulating Profits Is the New Way to Philanthropy Thanks to <strong>GoldFinX</strong>..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://www.ccn.com/why-accumulating-profits-is-the-new-way-to-philanthropy-thanks-to-goldfinx" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                <img src="./images/media/street-insider.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Feb 20, 2019</p>
                        <p class="title">Street Insider</p>
                        <p class="description">"The gold mining liquidity revolution carried by <strong>GoldFinX</strong> and its crypto solution..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="http://markets.financialcontent.com/streetinsider/news/read/37786839/" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                <img src="./images/media/investor-place.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Feb 20, 2019</p>
                        <p class="title">Investor Place</p>
                        <p class="description">"Gold and crypto combined in a single resource currently in presale brought by <strong>GoldFinX</strong>..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="http://markets.financialcontent.com/investplace/news/read/37786840/" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                <img src="./images/media/digital-journal.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">&nbsp;</p>
                        <p class="title">Digital Journal</p>
                        <p class="description">"The undeniable attributes of gold as a stable resource over the years and the enormous potential of digital assets..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="http://www.digitaljournal.com/pr/4169757" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                    <img src="./images/media/forbes.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Mar 27, 2019</p>
                        <p class="title">Forbes</p>
                        <p class="description">"<strong>GoldFinX</strong>: Blockchain security and Protected Coin value..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://www.forbes.com/sites/andrewarnold/2019/03/27/blockchain-security-how-far-have-we-come-in-2019/#58072d132457" target="_blank" class="btn btn-primary">+ Read More</a>
                        </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                <img src="./images/media/medium.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">Mar 01, 2019</p>
                        <p class="title">Medium</p>
                        <p class="description">"Here’s why you should rethink what comes to mind when you hear <strong>gold mine</strong>..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://blog.usejournal.com/heres-why-you-should-rethink-what-comes-to-mind-when-you-hear-gold-mine-c6d29f2a71bd" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                <img src="./images/media/coin-schedule.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">&nbsp;</p>
                        <p class="title">Coinschedule</p>
                        <p class="description">"<strong>GoldFinX</strong> receives the highest rating score of <strong>A</strong> on CoinSchedule..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://www.coinschedule.com/cryptocurrency/goldfinx" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                </div>
            </div>
            <div class="col-12 col-md-3 pb-4 d-flex align-items-stretch">
                <div class="card news-item">
                <img src="./images/media/ccn.jpg" class="card-img-top">
                    <div class="card-body">
                        <p class="date">&nbsp;</p>
                        <p class="title">CCN</p>
                        <p class="description">"<strong>GoldFinX</strong>: Empowering Artisanal Gold Mining and Legitimizing Crypto..."</p>
                        </div>
                        <div class="text-center card-footer">
                            <a href="https://www.ccn.com/goldfinx-empowering-artisanal-gold-mining-and-legitimizing-crypto/" class="btn btn-primary" target="_blank">+ Read More</a>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END NEWS SECTION -->
<?php include 'partials/footer.php'; ?>