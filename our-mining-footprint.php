<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

    <!-- START ABOUT SECTION -->
    <section class="home-featured align-items-end d-flex mining-footprint">
        <div class="container-fluid px-5 mb-5">
            <div class="row">
                <div class="col-12 col-md-8 wow fadeInUp">
                    <h1><strong>OUR MINING FOOTPRINT</strong></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                    <p class="callout-horizontal"></p>
                </div>
            </div>
    </section>
    <!-- END ABOUT SECTION -->

    <!-- START TEXT SECTION -->
    <section>
        <div class="container-fluid px-5">
            <div class="row">
                <div class="col-12 col-md-5 text-justify">
                    <p>Ideal mining projects for GoldFinX are small operations with big potential. We are prioritizing the alluvial and open-pit mining operations in our mine selection. Each project is thoroughly analyzed internally by our in-house experts and validated by our Advisory Board.</p>
<!--                    <p>Each mining operation selected should possess approximately 600,000 to 1,000,000+ Oz of gold in reserve. Once extracted to the end of the life of the mine, it should have generated (at today’s market price per Oz of €1,000) more than €1,000,000,000.</p>-->
                    <p>We have thus far identified over 75 prospective mines worldwide from which we will select at least 15 mines to activate Phase I of the business plan. The first production of Gold will be delivered by Q4 2019. The 15 selected mines in Phase 1 should eventually generate €15 billion of revenue and the GiX coin should be backed by €2.25 billion in Gold reserves in a Swiss vault. </p>
                    <p>However, the business model is completely scalable, and we will fluidly execute the plan by financing the hierarchy of potential gold mines as funds are being raised. Our forecast shows that the value of the underlying stored asset will match the face value of the GiX minted within 16 months.</p>
                </div>
                <div class="col-12 col-md-7">
                    <img src="images/mining-map.svg" class="img-miningmap img-fluid">
                </div>
            </div>
        </div>
    </section>
    <!-- END TEXT SECTION -->

<?php include 'partials/footer.php'; ?>