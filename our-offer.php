<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

<!-- START ABOUT SECTION -->
<section class="home-featured align-items-end d-flex GoldFactsHeader">
    <div class="container-fluid px-5 mb-5">
        <div class="row">
            <div class="col-12 col-md-8 wow fadeInUp">
                <h1><strong>OUR OFFER</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                <p class="callout-horizontal"></p>
            </div>
        </div>
</section>
<!-- END ABOUT SECTION -->

<!-- START BUSINESS SECTION -->
<section>
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-12">
                <div class="callout-left">
                    <h3><strong>GoldFinX</strong> is raising €250 Million selling GiX at <strong>€2</strong></h3>
                </div>
            </div>
        </div>

        <div class="row mt-4 text-justify">
            <div class="col-12 col-md-6">
                <p class="callout-left-child">The proceeds will cover the capital and operation costs during Phase 1 for at least 15 selected Gold mines worldwide. In Q4 2019, the GiX coin will be listed on major crypto exchanges, offering liquidity to its holders.</p>
<!--                <p class="callout-left-child">For a limited time only, selected investors will have the chance to acquire GiX coins at a discounted price. The proceeds will cover the capital and operation costs for 15 selected Gold mines in 12 different countries. </p>-->
                <p class="callout-left-child">The start of Gold extraction will start in Q4 2019, and will accumulate as well as stored indefinitely to back the value of the GiX coin. The 15+ mines should eventually generate €15 Billion of revenue and the GiX coin should be protected by €2.25 billion in Gold reserves.</p>
                <p class="callout-left-child">It is anticipated that the accumulation of Gold in the reserve, the rigorous execution of the plan, and media attention created will all have a positive effect on the GiX market price that will benefit its early subscribers.</p>
            </div>
            <div class="col-12 col-md-4 offset-md-1">
                <div class="text-center">
                    <img src="images/our-offer.svg" class="img-ouroffer img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END BUSINESS SECTION -->

<?php include 'partials/footer.php'; ?>