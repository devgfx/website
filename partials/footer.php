            <!-- START FOOTER SECTION -->
            <footer>
                <div class="container-fluid px-5">
                    <div class="row"> 
                        <div class="col-lg-2">
                            <img src="images/logo-white.svg" class="img-logowhite">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-12">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li><a href="fair-trade-crypto-finance.php">About Us</a></li>
                                <li><a href="media.php">News</a></li>
                                <li><a href="media.php">Media/Press</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-12">
                            <ul>
                                <li><a href="faq.php">FAQ</a></li>
                                <li><a href="#"  data-target="#register" data-behave="open-layer">Buy GiX</a></li>
                                <li><a href="https://admin.goldfinx.com/login" target="_blank">Login to Admin Dashboard</a></li>
                                <li><a href="https://vault.goldfinx.com/login" target="_blank">Login to Gix Wallet</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-12">
                            <ul>
                                <li><a href="https://www.youtube.com/channel/UCXhQah3zwRebtKmtD6fK49w?view_as=subscriber" target="_blank">Videos & Tutorials</a></li>
                                <li><a href="contact.php">Contact Us</a></li>
                                <li><a href="index.php#management-team">Management Team</a></li>
                                <li><a href="mailto:dev@goldfinx.com" target="_blank">Tech Support</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-12">
                            <ul>
                                <li><a href="documents/GoldFinX_whitepaper_V2.3.pdf" target="_blank">Whitepaper</a></li>
                                <li><a href="documents/GoldFinX_privacy_policy_eu.pdf" target="_blank">Privacy Policy - EU</a></li>
                                <li><a href="documents/GoldFinX_privacy_policy_non_eu.pdf" target="_blank">Privacy Policy - Non EU</a></li>
                                <li><a href="documents/GoldFinX_terms_and_conditions.pdf" target="_blank">Terms & Conditions</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-12 newsletter">
                            <h5>Newsletter</h5>
                            <!-- Begin Mailchimp Signup Form -->
                            <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
                            <div id="mc_embed_signup">
                                <form action="https://gmail.us20.list-manage.com/subscribe/post?u=4a68b07336f4a437d992f8913&amp;id=0e9242d20d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">
                                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Insert your e-mail for updates" required>
                                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4a68b07336f4a437d992f8913_0e9242d20d" tabindex="-1" value=""></div>
                                        <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                    </div>
                                </form>
                            </div>
                            <!--End mc_embed_signup-->
                            <ul class="social-networks">
                                <li><a href="http://www.facebook.com/goldfinx" target="_blank"><img src="images/social/facebook.svg"></a></li>
                                <li><a href="https://www.instagram.com/goldfinx/" target="_blank"><img src="images/social/instagram.svg"></a></li>
                                <li><a href="http://www.linkedin.com/company/goldfinx" target="_blank"><img src="images/social/linkedin.svg"></a></li>
                                <li><a href="http://t.me/GoldFinX" target="_blank"><img src="images/social/telegram.svg"></a></li>
                                <li><a href="http://www.twitter.com/goldfinx" target="_blank"><img src="images/social/twitter.svg"></a></li>
                                <li><a href="https://www.weibo.com/u/7234979455?is_hot=1" target="_blank"><img src="images/social/weibo.svg"></a></li>
                                <li><a href="https://www.youtube.com/channel/UCXhQah3zwRebtKmtD6fK49w?view_as=subscriber" target="_blank"><img src="images/social/youtube.svg"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- END FOOTER SECTION -->
        </div>
        <!-- END CONTENT -->
        <?php include 'scripts.php'; ?>
    </body>
</html>