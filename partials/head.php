<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" type="image/x-icon" href="images/gix_favicon.ico">
        <meta content="GoldFinX" name="author">
        <META name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="description" content="Join our Protected Coin Offering and put gold mines in your wallet.">
        <meta property="og:image" content="https://goldfinx.com/images/goldfinx-og.png?version=2" />
        <meta property="og:title" content="GoldFinX" />
        <meta name="og:description" content="Join our Protected Coin Offering and put gold mines in your wallet.">
        <meta name="keywords" content="">
        <title>GoldFinX - Empowering Artisanal Gold Mining</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/goldfinx.css?version=2.9.1">
    </head>
    <body>