<!-- START HEADER -->
<header>
    <!-- START NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top px-0 py-4">
        <div class="container-fluid px-5">
            <a class="navbar-brand" href="index.php">
                <img src="images/logo-goldfinx.svg" class="img-logogoldfinx">
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-item nav-link dropdown-toggle"  data-toggle="dropdown" href="#about">ABOUT</a>
                        <div class="dropdown-menu" id="about"><a class="dropdown-item" href="gix-unique-concept.php">GiX UNIQUE CONCEPT</a>
                            <a class="dropdown-item" href="our-offer.php">OUR OFFER</a>
                            <a class="dropdown-item" href="fair-trade-crypto-finance.php">FAIR TRADE CRYPTO-FINANCE</a>
                            <a class="dropdown-item" href="our-mining-footprint.php">OUR MINING FOOTPRINT</a>
                            <a class="dropdown-item" href="social-environmental-responsibility.php">SOCIAL & ENVIRONMENTAL RESPONSIBILITY</a>
                            <a class="dropdown-item" href="coin-sale-term-sheet.php">COIN SALE TERM SHEET</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-target="#news" data-toggle="dropdown" href="#">NEWS</a>
                        <div class="dropdown-menu" id="news"><a class="dropdown-item" href="media.php">GoldFinX in the News</a>
                        <!-- <a class="dropdown-item" target="_blank" href="news.php">GoldFinX Updates</a> -->
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-target="#whitepaper" data-toggle="dropdown" href="#">WHITEPAPER</a>
                        <div class="dropdown-menu" id="whitepaper"><a class="dropdown-item" target="_blank" href="documents/GoldFinX_whitepaper_V2.3.pdf">ENGLISH</a><label class="dropdown-header">Executive Summaries</label><a class="dropdown-item" target="_blank" href="documents/summaries/goldfinx_fr.pdf">FRANÇAIS</a>
                            <a class="dropdown-item" target="_blank" href="documents/summaries/goldfinx_de.pdf">DEUTSCH</a>
                            <a class="dropdown-item" target="_blank" href="documents/summaries/goldfinx_ar.pdf"> عربي</a>
                            <a class="dropdown-item" target="_blank" href="documents/summaries/goldfinx_ru.pdf">PУССКИЙ</a>
                            <a class="dropdown-item" target="_blank" href="documents/summaries/goldfinx_es.pdf">ESPAÑOL</a>
                            <a class="dropdown-item" target="_blank" href="documents/summaries/goldfinx_pt.pdf">PORTUGUÊS</a>
                            <a class="dropdown-item" target="_blank" href="documents/summaries/goldfinx_cn.pdf">中文</a>
                            <a class="dropdown-item" target="_blank" href="documents/summaries/goldfinx_tk.pdf">Türkçe</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="faq.php">FAQ</a>
                    </li>
                    <li class="nav-item d-lg-none">
                        <button type="button" class="btn btn-primary btn-block my-3" data-target="#login" data-behave="open-layer">LOGIN</button>
                    </li>
                    <li class="nav-item d-lg-none">
                        <button type="button" class="btn btn-warning btn-block" data-target="#register" data-behave="open-layer">BUY <img src="images/gix-btn.svg" class="img-gixbtn"></button>
                    </li>
                </ul>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavRight">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <button type="button" class="btn btn-primary" data-target="#login" data-behave="open-layer">LOGIN</button>
                    </li>
                    <li class="nav-item">
                        <button type="button" class="btn btn-warning" data-target="#register" data-behave="open-layer">BUY <img src="images/gix-btn.svg" class="img-gixbtn"></button>
                    </li>
                    <!--<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-target="#languages" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">EN</a>
                        <div class="dropdown-menu" id="languages">
                            <a class="dropdown-item" href="#">CN</a>
                            <a class="dropdown-item" href="#">DE</a>
                            <a class="dropdown-item" href="#">KR</a>
                        </div>
                    </li>-->
                </ul>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
</header>
<!-- END HEADER -->

<!-- START LOGIN -->
<div class="login-overlay" id="login">
    <div class="container-fluid">
        <div class="row justify-content-center text-center">
            <div class="col-md-4 mb-5">
                <p>Manage Wallet?</p>
                <a href="https://vault.goldfinx.com/login" target="_blank" class="btn btn-warning">LOGIN TO <span><img src="images/gix-btn.svg" class="img-gixbtn"> WALLET</span></a>
            </div>
            <div class="col-md-4">
                <p>Manage Network?</p>
                <a href="https://admin.goldfinx.com/login" target="_blank" class="btn btn-primary">LOGIN TO <span>ADMIN DASHBOARD</span></a>
                <p class="mt-3">For Affiliate Networks with<br>Admin credentials only</p>
            </div>
        </div>
    </div>
    <img src="images/icon-close-overlay.svg" class="img-iconcloseoverlay"  data-target="#login" data-behave="close-layer">
</div>
<!-- END LOGIN -->

<!-- START register -->
<div class="login-overlay" id="register">
    <div class="container-fluid">
        <div class="row justify-content-center text-center">
            <div class="col-md-4 mb-5">
                <p>Account Holder?</p>
                <a href="https://vault.goldfinx.com/login" target="_blank" class="btn btn-warning">LOGIN AND BUY <img src="images/gix-btn.svg" class="img-gixbtn"></a>
            </div>
            <div class="col-md-4">
                <p>Not Registered?</p>
                <a href="https://vault.goldfinx.com/register" target="_blank" class="btn btn-primary">CREATE ACCOUNT</a>
            </div>
        </div>
    </div>
    <img src="images/icon-close-overlay.svg" class="img-iconcloseoverlay"  data-target="#register" data-behave="close-layer">
</div>
<!-- END REGISTER -->

<!-- START CONTENT -->
    <div class="main-content">