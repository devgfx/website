<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>

<!-- START ABOUT SECTION -->
<section class="home-featured align-items-end d-flex social-mine">
    <div class="container-fluid px-5 mb-5">
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInUp">
                <h1><strong>SOCIAL & ENVIRONMENTAL RESPONSIBILITY</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                <p class="callout-horizontal"></p>
            </div>
        </div>
</section>
<!-- END ABOUT SECTION -->

<!-- START RESPONSIBILITY SECTION -->
<section>
    <div class="container-fluid px-5">
        <div class="row mb-5">
            <div class="col-12 col-md-6 wow fadeInUp text-justify">
                <p>Improving the lifestyle of miners and their families through GoldFinX is not charity. It is an extremely good business model that not only rewards the risk taken by initial coin holders, but also benefits gold miners while positively impacting rural communities both socially and environmentally.</p>
                <p>GoldFinX’s social and environmentally conscious efforts ensure the crypto-finance opportunity benefits everyone within the cycle of the project. To this effect, we have structured an important compulsory “Social Impact Program” around the funding of each mining operation.</p>
            </div>
            <div class="col-12 col-md-6">
                <img src="images/coin-front-mono.png" class="img-coinfrontmono in-social-mine">
            </div>
        </div>
        <div class="row flex-row-reverse">
            <div class="col-12 col-md-6 ">
                <div class="callout-right text-right wow fadeInRight">
                    <h3><strong>Protecting the environment</strong> is at the root of the <strong>GoldFinX</strong> project</h3>
                </div>
                <div class="callout-right-child wow fadeInUp text-justify" data-wow-delay="1s">
                    <p>We only focus on existing gold mines by helping them to employ socially conscious and sustainable mining methods that reduce the negative impact on the surrounding ecosystems.</p>
                    <p>We strongly support Non-Governmental Organizations like WWF, particularly in their efforts to prevent destruction of rainforests, the exploitation and forced eviction of natives, and the pollution of rivers and oceans with cyanide and mercury. </p>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="text-center">
                    <div class="img-socialinfant  wow fadeInUp">
                        <img src="images/social-infant.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END RESPONSIBILITY SECTION -->

<?php include 'partials/footer.php'; ?>